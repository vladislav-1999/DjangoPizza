from django.shortcuts import render


def index(request):
    info = [
        ['Карбонара', 'https://github.com/yodo-im/python_pizza/blob/main/pizza/carbonara.jpeg?raw=true',
         'Бекон, сыры чеддер и пармезан, моцарелла, томаты черри, красный лук, чеснок, соус альфредо, итальянские травы',
         300],

        ['Четыре сыра', 'https://github.com/yodo-im/python_pizza/blob/main/pizza/four_cheeses.jpeg?raw=true',
         'Сыр блю чиз, сыры чеддер и пармезан, моцарелла, соус альфредо',
         450],

        ['Ветчина и сыр', 'https://github.com/yodo-im/python_pizza/blob/main/pizza/ham_and_cheese.jpeg?raw=true',
         'Ветчина, моцарелла, соус альфредо',
         600],

        ['Ветчина и грибы', 'https://github.com/yodo-im/python_pizza/blob/main/pizza/ham_and_mushrooms.jpeg?raw=true',
         'Ветчина, шампиньоны, увеличенная порция моцареллы, томатный соус',
         350],

        ['Пепперони', 'https://github.com/yodo-im/python_pizza/blob/main/pizza/pepperoni.jpeg?raw=true',
         'Пикантная пепперони, увеличенная порция моцареллы, томатный соус',
         400],
    ]
    return render(request, 'mainapp/index.html', {'info': info})


def basket_page(request):
    return render(request, 'mainapp/basket_page.html')

